import {
    IonActionSheet, IonAlert, IonButtons,
    IonCard, IonCardHeader, IonCardTitle,
    IonContent, IonFab, IonFabButton, IonHeader,
    IonIcon, IonItem, IonList, IonMenuButton,
    IonPage, IonTitle, IonToolbar, IonCardSubtitle, IonCardContent,
    IonGrid, IonRow, IonCol, IonLabel, IonButton
} from '@ionic/react';
import React, {useEffect, useState} from 'react';
import {isPlatform} from '@ionic/react';
import {useHistory} from "react-router";
import {opciones} from "./Data";

const Letra = ({letra}: {letra: string}) => {
    const [color, setColor] = useState<string>('primary');
    return (
        <IonButton
            color={color}
            onClick={() => setColor('warning' == color? 'primary': 'warning')}
            style={
                {
                    width: '10%',
                    height: '50px'
                }
            }
        >
            {letra}
        </IonButton>
    );
};

export default Letra;

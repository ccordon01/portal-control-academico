export const opciones = [
    {
        opcion: "A",
        letras: [
            'A',
            'E',
            'I',
            'O',
            'U'
        ],
        image: 'https://i.pinimg.com/originals/53/7f/82/537f8244fd0a1284642d771262e7a18a.png'
    },
    {
        opcion: "E",
        letras: [
            'A',
            'E',
            'I',
            'O',
            'U'
        ],
        image: 'https://i.pinimg.com/originals/ec/46/31/ec4631fe89228ff3a7e89601cd2540b1.png'
    },
    {
        opcion: "I",
        letras:[
            'A',
            'E',
            'I',
            'O',
            'U'
        ],
        image: 'https://images.vexels.com/media/users/3/196065/isolated/lists/81c6d748b4d5407e934c013744e174b4-arctic-igloo-illustration.png'
    },
    {
        opcion: "O",
        letras:[
            'A',
            'E',
            'I',
            'O',
            'U'
        ],
        image: 'https://i.pinimg.com/originals/bf/f3/c0/bff3c0729efe69835ea23653312bb1d2.png'
    },
    {
        opcion: "U",
        letras: [
            'A',
            'E',
            'I',
            'O',
            'U'
        ],
        image: 'https://deconejo.site/wp-content/uploads/2019/12/uvas-tiernas.png'
    },
]
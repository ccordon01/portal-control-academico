import {
    IonActionSheet, IonAlert, IonButtons,
    IonCard, IonCardHeader, IonCardTitle,
    IonContent, IonFab, IonFabButton, IonHeader,
    IonIcon, IonItem, IonList, IonMenuButton,
    IonPage, IonTitle, IonToolbar, IonCardSubtitle, IonCardContent,
    IonGrid, IonRow, IonCol, IonLabel, IonButton
} from '@ionic/react';
import React, {useEffect, useState} from 'react';
import {isPlatform} from '@ionic/react';
import {useHistory} from "react-router";
import {opciones} from "./Data";
import Letra from "./letra";

const Vocales: React.FC = () => {
    let history = useHistory();
    const [letra, setletra] = useState<{
        opcion: string,
        letras: Array<string>,
        image: string
    }>({
        opcion: '',
        letras: [],
        image: ''
    });
    const name = 'Las Vocales';

    useEffect(() => {
        newVocal();
    }, []);

    function newVocal() {
        let i = Math.floor(Math.random() * 5);
        setletra(opciones[i]);
    }

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton/>
                    </IonButtons>
                    <IonTitle>{name}</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent>
                <IonHeader collapse="condense">
                    <IonToolbar>
                        <IonTitle size="large">{name}</IonTitle>
                    </IonToolbar>
                </IonHeader>

                {
                    letra ?
                        <IonCard
                            style={
                                {
                                    height: '60vh'
                                }
                            }
                        >
                            <IonCardHeader
                                style={
                                    {
                                        textAlign: 'center'
                                    }
                                }
                            >
                                <span
                                    style={
                                        {
                                            fontSize: '3em'
                                        }
                                    }
                                > Adivina la Letra </span>
                            </IonCardHeader>
                            <img style={
                                {
                                    width: '35%',
                                    marginLeft: '30%',
                                    height: 'auto'
                                }
                            } src={letra.image}/>
                        </IonCard>
                        : <div/>
                }
                <div style={
                    {
                        textAlign: 'center'
                    }
                }>
                    <IonButton
                        onClick={() => newVocal()}
                    >
                        OTRA
                    </IonButton>
                </div>
                <div
                    style={
                        {
                            textAlign: 'center'
                        }
                    }
                >
                    {
                        letra ?
                            letra.letras.map(
                                l => {
                                    return (
                                        <Letra
                                            letra={l}/>
                                    )
                                }
                            ):
                            <div/>
                    }
                </div>
            </IonContent>
        </IonPage>
    );
};

export default Vocales;

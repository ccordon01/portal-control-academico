import {
    IonButtons,
    IonContent,
    IonHeader,
    IonMenuButton,
    IonPage,
    IonTitle,
    IonToolbar,
    IonList,
    IonItem, IonIcon, IonLabel, IonButton
} from '@ionic/react';
import React from 'react';
import './Juegos.css';
import {fastFoodOutline, fastFoodSharp, gameController} from "ionicons/icons";
import {useHistory} from "react-router";

const Games: React.FC = () => {
    const name = "Juegos";
    const history = useHistory();

    function addGame(route: string) {
        localStorage.setItem('onGame', '1');
        history.push(`juegos${route}`);
    }

    const games = [
        {
            name: 'Sopa de Letras',
            route: '/sopa',
            iosIcon: fastFoodOutline,
            mdIcon: fastFoodSharp
        },
        {
            name: 'Las Vocales',
            route: '/vocales',
            iosIcon: fastFoodOutline,
            mdIcon: fastFoodSharp
        }
    ]

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton/>
                    </IonButtons>
                    <IonTitle>{name}</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent>
                <IonHeader collapse="condense">
                    <IonToolbar>
                        <IonTitle size="large">{name}</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonList style={
                    {
                        textAlign: 'center',
                        marginTop: '20px'
                    }
                }>
                    {
                        games.map((game, index) => {
                            return (
                                <IonButton
                                    shape="round"
                                    style={
                                        {
                                            width: '85%',
                                            maxWidth: '530px',
                                            height: '60px'
                                        }
                                    }
                                    onClick={() => addGame(game.route)}>
                                    <IonIcon slot="start" ios={game.iosIcon} md={game.mdIcon}/>
                                    <IonIcon slot="end" ios={game.iosIcon} md={game.mdIcon}/>
                                    <IonLabel>{game.name}</IonLabel>
                                </IonButton>
                            );
                        })
                    }
                </IonList>
            </IonContent>
        </IonPage>
    );
};

export default Games;
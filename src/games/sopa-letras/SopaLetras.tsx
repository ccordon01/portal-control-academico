import {
    IonActionSheet, IonAlert, IonButtons,
    IonCard, IonCardHeader, IonCardTitle,
    IonContent, IonFab, IonFabButton, IonHeader,
    IonIcon, IonItem, IonList, IonMenuButton,
    IonPage, IonTitle, IonToolbar, IonCardSubtitle, IonCardContent,
    IonGrid, IonRow, IonCol, IonLabel, IonButton
} from '@ionic/react';
import React, {useEffect, useState} from 'react';
import './SopaLetras.css';
import {isPlatform} from '@ionic/react';
import {useHistory} from "react-router";
import {opciones} from "./Data";
import Letra from "./letra";

const SopaLetras: React.FC = () => {
    let history = useHistory();
    const [palabra, setPalabra] = useState<string>('');
    const name = 'Sopa de Letras';

    const [rows, setRows] = useState<Array<Array<string>>>([]);
    const filas: Array<Array<string>> = [];

    function makeRow(length: number, palabra: string = ""): Array<string> {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            if(i >= palabra.length) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }else{
                console.log(palabra.charAt(i));
                result += palabra.charAt(i);
            }
        }
        return result.split('');
    }

    // useEffect(() =>{
    //     for(let i = 0; i<6; i++) {
    //         filas.push(makeRow(6));
    //     }
    //     setRows(filas);
    // }, []);

    function getPalabras(index: number) {
        let tmp = opciones[index].letras.splice(0, 1).map(palabra => palabra.toUpperCase())[0];
        setPalabra(tmp ? tmp : '');
        let rowOrCol = Math.floor(Math.random() * 2);
        let inx = Math.floor(Math.random() * 6);
        for (let i = 0; i < 6; i++) {
            if (inx == i) {
                filas.push(makeRow(6, tmp));
            } else {
                filas.push(makeRow(6));
            }
        }
        setRows(filas);
    }

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton/>
                    </IonButtons>
                    <IonTitle>{name}</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent>
                <IonHeader collapse="condense">
                    <IonToolbar>
                        <IonTitle size="large">{name}</IonTitle>
                    </IonToolbar>
                </IonHeader>

                <div style={
                    {
                        textAlign: 'center'
                    }
                }>
                    <div style={
                        {
                            width: '100%',
                            height: '30px',
                            marginTop: '10px'
                        }
                    } id="opciones">
                        {
                            opciones.map((opcion, index) => {
                                return (
                                    <IonButton
                                        onClick={() => getPalabras(index)}
                                        shape="round">
                                        {opcion.opcion}
                                    </IonButton>
                                )
                            })
                        }
                    </div>

                    <div id="sopa"
                         style={
                             {
                                 textAlign: 'center',
                                 width: '100%',
                                 marginTop: '4%'
                             }
                         }
                    >
                        {
                            rows.map(fila => {
                                return (
                                    <div style={
                                        {
                                            width: '100%'
                                        }
                                    }>
                                        {
                                            fila.map(columna => {
                                                return (
                                                   <Letra letra={columna}/>
                                                );
                                            })
                                        }
                                    </div>
                                );
                            })
                        }
                    </div>

                    <div
                        style={
                            {
                                width: '100%',
                                textAlign: 'center',
                                marginTop: '3%'
                            }
                        }
                    >
                            <span
                                style={
                                    {
                                        marginTop: '3%',
                                        width: '33%',
                                        marginLeft: '10%',
                                        textAlign: 'center',
                                        margin: '50px'
                                    }
                                }
                            >
                            {`                   ${palabra}                \t`}
                            </span>
                    </div>
                </div>
            </IonContent>
        </IonPage>
    );
};

export default SopaLetras;

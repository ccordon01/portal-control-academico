export const opciones = [
    {
        opcion: "Oceano",
        letras: [
            'pez',
            'pulpo',
            'alga',
            'delfin'
        ]
    },
    {
        opcion: "Safari",
        letras: [
            'leon',
            'mono',
            'tigre',
            'oso',
            'cebra',

        ]
    },
    {
        opcion: "Domésticos",
        letras: [
            'gato',
            'perro',
            'loro',
            'pollo',
        ]
    },
    {
        opcion: "Granja",
        letras: [
            'gallo',
            'vaca',
            'cerdo',
            'pato',
            'pavo'
        ]
    },
]
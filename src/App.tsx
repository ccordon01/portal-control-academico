import Menu from './components/Menu';
import React from 'react';
import { IonApp, IonRouterOutlet, IonSplitPane } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { Redirect, Route } from 'react-router-dom';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import Info from "./pages/Info/Info";
import Login from './pages/Login/Login'
import Alumno from "./pages/Alumnos/Editar/Alumno";
import ListarA from "./pages/Alumnos/Listar/listarAlumno";
import CrearAlumno from "./pages/Alumnos/Crear/crearAlumno";
import Maestros from "./pages/Maestros/Maestros";
import Maestro from "./pages/Maestros/Maestro/Maestro";
import Home from "./pages/Home/Home";
import NuevoMaestro from "./pages/Maestros/NuevoMaestro/NuevoMaestro";
import CrearInstitucion from "./pages/Instituciones/Crear/crearInstitucion";
import ListarI from "./pages/Instituciones/Listar/listarInstituciones";
import EditarI from "./pages/Instituciones/Editar/editarInstitucion";
import Selection_ from "./pages/Actividades/seleccion";
import Seleccion from "./pages/Actividades/seleccion";
import Padres from "./pages/Padres/Padres";
import Padre from "./pages/Padres/Padre/Padre";
import NuevoPadre from "./pages/Padres/NuevoPadre/NuevoPadre";
import CrearActividad from "./pages/Actividades/crear/actividad";
import EditarActividad from "./pages/Actividades/editar/editarActividad";
import Grados from "./pages/Grados/Grados";
import Secciones from "./pages/Secciones/Secciones";
import HomeProfesor from "./HomeProfesor/HomeProfesor";
import SopaLetras from "./games/sopa-letras/SopaLetras";
import Games from "./games/Juegos";
import Vocales from "./games/vocales/Vocales";

const App: React.FC = () => {

  return (
    <IonApp>
      <IonReactRouter>
        <IonSplitPane contentId="main">
          <Menu />
          <IonRouterOutlet id="main">
            <Route path="/secciones/:id" component={Secciones} exact />
            <Route from="/secciones" to="/grados" exact />
            <Route path="/grados" component={Grados} exact/>
            <Redirect from="/grado" to="/grados" exact />
            <Route path="/editarActividad/:id" component={EditarActividad} exact />
            <Route path="/crearActividad" component={CrearActividad} exact />
            <Route path="/listarActividades" component={Seleccion} exact />
            <Route path="/editarInstitucion/:id" component={EditarI} exact />
            <Route path="/listarInstituciones" component={ListarI} exact />
            <Route path="/crearInstitucion" component={CrearInstitucion} exact />
            <Route path="/crearAlumno" component={CrearAlumno} exact />
            <Route path="/listaralumno" component={ListarA} exact />
            <Route path="/Alumno/:id" component={Alumno} exact />
            <Route path="/padres/nuevo" component={NuevoPadre} exact />
            <Route path="/padre/:id" component={Padre} exact />
            <Route path="/padres" component={Padres} exact />
            <Redirect from="/padre" to="/padres" exact />
            <Route path="/maestros/nuevo" component={NuevoMaestro} exact/>
            <Route path="/maestro/:id" component={Maestro} exact />
            <Route path="/maestros" component={Maestros} exact />
            <Redirect from="/maestro" to="/maestros" exact />
            <Route path="/info" component={Info} exact />
            <Route path="/login" component={Login} exact />
            <Redirect from="/" to="/info" exact />
            <Route path="/home" component={Home} exact />
            <Route path="/homeProfesor" component={HomeProfesor} exact />
            <Route path="/juegos" component={Games} exact />
            <Route path="/juegos/sopa" component={SopaLetras} exact />
            <Route path="/juegos/vocales" component={Vocales} exact />
          </IonRouterOutlet>
        </IonSplitPane>
      </IonReactRouter>
    </IonApp>
  );
};

export default App;

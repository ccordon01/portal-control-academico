import {
    IonContent,
    IonIcon, IonImg,
    IonItem,
    IonLabel,
    IonList,
    IonListHeader,
    IonMenu,
    IonMenuToggle,
    IonNote,
    IonItemGroup, IonItemDivider
} from '@ionic/react';

import React from 'react';
import {useHistory, useLocation} from 'react-router-dom';
import {
    fileTrayStackedOutline, fileTrayStackedSharp, gameController,
    informationOutline,
    informationSharp,
    logInOutline,
    logOutOutline,
} from 'ionicons/icons';
import {
    homeOutline,
    homeSharp,
    schoolOutline,
    schoolSharp,
    bookOutline,
    bookSharp,
    peopleOutline, peopleSharp
} from 'ionicons/icons';
import './Menu.css';

interface AppPage {
    url: string;
    iosIcon: string;
    mdIcon: string;
    title: string;
}

const appPagesLogin: AppPage[] = [
    {
        title: 'Home',
        url: '/home',
        iosIcon: homeOutline,
        mdIcon: homeSharp
    }, {
        title: 'Home Profesor',
        url: '/homeProfesor',
        iosIcon: homeOutline,
        mdIcon: homeSharp
    }, {
        title: 'Juegos',
        url: '/juegos',
        iosIcon: gameController,
        mdIcon: gameController
    }, {
        title: 'Info',
        url: '/info',
        iosIcon: informationOutline,
        mdIcon: informationSharp
    }, {
        title: 'Padres',
        url: '/padres',
        iosIcon: peopleOutline,
        mdIcon: peopleSharp
    },
    {
        title: 'Maestros',
        url: '/maestros',
        iosIcon: schoolOutline,
        mdIcon: schoolSharp
    },
    {
        title: 'Alumnos',
        url: '/listaralumno',
        iosIcon: bookOutline,
        mdIcon: bookSharp
    },
    {
        title: 'Instituciones',
        url: '/listarInstituciones',
        iosIcon: schoolOutline,
        mdIcon: schoolSharp
    },
    {
        title: 'Actividades',
        url: '/listarActividades',
        iosIcon: bookOutline,
        mdIcon: bookSharp
    },
    {
        title: 'Grados',
        url: '/grados',
        iosIcon: fileTrayStackedOutline,
        mdIcon: fileTrayStackedSharp
    }
];

const appPages: AppPage[] = [
    {
        title: 'Info',
        url: '/info',
        iosIcon: informationOutline,
        mdIcon: informationSharp
    }, {
        title: 'Login',
        url: '/login',
        iosIcon: logInOutline,
        mdIcon: logInOutline
    }
];

const Menu: React.FC = () => {
    const location = useLocation();
    let history = useHistory();

    let login = false;
    const localUser = localStorage.getItem('token');
    login = localUser != null;

    const onGame = localStorage.getItem('onGane');


    if (login) {
        return (
            <IonMenu className="no-border-shadow" contentId="main" type="overlay">
                <IonContent>
                    <IonImg style={{
                        height: '10vh',
                    }} src='./assets/img/logo.png'/>
                    <IonListHeader>Portal Control Academico</IonListHeader>
                    <IonNote>portalacademico@mail.com</IonNote>
                    {appPagesLogin.map((appPage, index) => {
                        return (
                            <IonMenuToggle key={index} autoHide={false}>
                                <IonItem className={location.pathname === appPage.url ? 'selected' : ''}
                                         routerLink={appPage.url} routerDirection="none" lines="none" detail={false}>
                                    <IonIcon slot="start" ios={appPage.iosIcon} md={appPage.mdIcon}/>
                                    <IonLabel>{appPage.title}</IonLabel>
                                </IonItem>
                            </IonMenuToggle>
                        );
                    })}
                    <IonItem onClick={() => {
                        console.log("LOGOUT");
                        history.push('/login');
                        localStorage.removeItem('token');
                    }}>
                        <IonIcon slot="start" ios={logOutOutline} md={logOutOutline}/>
                        <IonLabel>Logout</IonLabel>
                    </IonItem>

                </IonContent>

            </IonMenu>
        );
    } else {
        return (
            <IonMenu className="no-border-shadow" contentId="main" type="overlay">
                <IonContent>
                    <IonImg style={{
                        height: '10vh',
                    }} src='./assets/img/logo.png'/>
                    <IonListHeader>Portal Control Academico</IonListHeader>
                    <IonNote>portalacademico@mail.com</IonNote>
                    {appPages.map((appPage, index) => {
                        return (
                            <IonMenuToggle key={index} autoHide={false}>
                                <IonItem className={location.pathname === appPage.url ? 'selected' : ''}
                                         routerLink={appPage.url} routerDirection="none" lines="none" detail={false}>
                                    <IonIcon slot="start" ios={appPage.iosIcon} md={appPage.mdIcon}/>
                                    <IonLabel>{appPage.title}</IonLabel>
                                </IonItem>
                            </IonMenuToggle>
                        );
                    })}
                </IonContent>
            </IonMenu>

        );
    }

    return (
        <div></div>
    );

};

export default Menu;

import './ImgFromInitials.css';
import React, {useState} from "react";

const ImgFromInitials = ({text, fontSize, rounded}: { text: string, fontSize: number, rounded: boolean }) => {
    let [r, setR] = useState(rounded ? '50%' : '0%');
    let [clss, setClss] = useState("cont " + getColor());
    let height:any = '100%';
    let width:any = '100%';
    let fontS = fontSize;
    if(rounded){
        height = fontSize;
        width = fontSize;
        fontS = fontSize -10;
    }
    return (
        <div style={{height: height, width: width, margin: "auto"}}>
            <div className={clss}
                 style={
                     {
                         fontSize: fontS,
                         borderRadius: r,
                     }
                 }>
                <div className="txt">
                    {getInitials(text)}
                </div>
            </div>
        </div>
    );
};

function getColor() {
    const options = ['one', 'two', 'tree', 'four'];
    const option = options[(Math.floor(Math.random() * 5))];
    return option === undefined ? 'two' : option;
}

function getInitials(text: string): string {
    const words = text.split(' ');
    let i1 = words[0];
    let i2 = words[1];
    i1 = i1 === undefined || i1 === '' ? ' ' : i1[0].toUpperCase();
    i2 = i2 === undefined || i2 === '' ? ' ' : i2[0].toUpperCase();
    return `${i1}${i2}`;
}

export default ImgFromInitials;
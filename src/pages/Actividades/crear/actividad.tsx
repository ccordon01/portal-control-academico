import {
    IonBackButton,
    IonLabel,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonItem,
    IonButtons,
    IonContent,
    IonHeader,
    IonMenuButton,
    IonPage,
    IonTitle,
    IonToolbar,
    IonInput,
    IonButton,
    IonDatetime,
    IonSelectOption, IonSelect
} from '@ionic/react';
import React, { useState } from 'react';
import './actividad.css';
import ImgFromInitials from "../../../components/ImgFromInitials";

const CrearActividad: React.FC = () => {
    const name = "Crear Actividad";

    const [nombreActividad, setNombreActividad] = useState('')
    const [descripcionActividad, setDescripcionActividad] = useState('')
    const [fechaInicio, setFechaInicio] = useState('')
    const [fechaFin, setFechaFin] = useState('')
    const [grado, setGrado] = useState('')
    const [seccion, setSeccion ] = useState('')
    const [save, setsave] = useState(false)

    if (save) {
        const Actividad = {
            nombreActividad: nombreActividad,
            descripcionActividad: descripcionActividad,
            fechaInicio: fechaInicio,
            fechaFin: fechaFin,
            grado: grado,
            seccion: seccion
        }
    }
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonBackButton/>
                    </IonButtons>
                    <IonTitle>{name}</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent>
                <IonHeader collapse="condense">
                    <IonToolbar>
                        <IonTitle size="large">{name}</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonCard style={
                    {
                        top: '50%',
                        position: 'absolute',
                        transform: 'translateY(-50%)',
                        width: '95%',
                        margin: '2.5%',
                        overflow: 'visible'
                    }}>
                    <div style={{
                        textAlign: 'center',
                        marginTop: '-10vh',
                    }}>
                        <div className='div-img' style={{position: "relative", width: '100%'}}>
                            <ImgFromInitials text={nombreActividad} fontSize={150} rounded={true}/>
                        </div>
                    </div>
                    <IonCardHeader>
                        <IonCardTitle style = {{textAlign: 'center'}}>Creacion de Actividad</IonCardTitle>
                    </IonCardHeader>
                    <IonItem>
                        <IonLabel>Nombre de la Actividad</IonLabel>
                        <IonInput value={nombreActividad} onIonChange={e => setNombreActividad(e.detail.value!)} placeholder="Nombre de la Actividad"></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel>Descripcion de la actividad</IonLabel>
                        <IonInput value={descripcionActividad} onIonChange={e => setDescripcionActividad(e.detail.value!)} placeholder="Direccion De la Institucion"></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel>Fecha de Inicio y hora</IonLabel>
                        <IonDatetime placeholder="D MMM YYYY H:m" displayFormat="D MMM YYYY H m" min="1940" value={fechaInicio}
                                     onIonChange={e => setFechaInicio(e.detail.value!)}></IonDatetime>
                    </IonItem>
                    <IonItem>
                        <IonLabel>Fecha de Fin y hora</IonLabel>
                        <IonDatetime placeholder="D MMM YYYY H:m" displayFormat="D MMM YYYY H m" min="1940" value={fechaFin}
                                     onIonChange={e => setFechaFin(e.detail.value!)}></IonDatetime>
                    </IonItem>
                    <IonItem>
                        <IonLabel>Grado</IonLabel>
                        <IonSelect
                            interface="alert"
                            okText="Seleccionar" cancelText="Cancelar" value={grado}
                            placeholder="Catedratico Principal"
                            onIonChange={e => setGrado(e.detail.value)}>
                            <IonSelectOption value="5">5to</IonSelectOption>
                            <IonSelectOption value="4">4to</IonSelectOption>
                            <IonSelectOption value="3">3ro</IonSelectOption>
                            <IonSelectOption value="2">2do</IonSelectOption>
                            <IonSelectOption value="1">1ro</IonSelectOption>
                        </IonSelect>
                    </IonItem>
                    <IonItem>
                        <IonLabel>Seccion</IonLabel>
                        <IonSelect
                            interface="alert"
                            okText="Seleccionar" cancelText="Cancelar" value={seccion}
                            placeholder="Catedratico Principal"
                            onIonChange={e => setSeccion(e.detail.value)}>
                            <IonSelectOption value="0">"A"</IonSelectOption>
                            <IonSelectOption value="1">"B"</IonSelectOption>
                            <IonSelectOption value="2">"C"</IonSelectOption>
                            <IonSelectOption value="3">"D"</IonSelectOption>
                        </IonSelect>
                    </IonItem>
                    <div style={{textAlign: 'center'}}>
                        <IonButton color="warning" shape="round" onClick={() => setsave(true)}>Crear</IonButton>
                    </div>
                </IonCard>
            </IonContent>
        </IonPage>
    );
};

export default CrearActividad;

import {
    IonCheckbox,
    IonCardContent,
    IonCardSubtitle,
    IonGrid,
    IonRow,
    IonCol,
    IonBackButton,
    IonLabel,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonItem,
    IonButtons,
    IonContent,
    IonHeader,
    IonMenuButton,
    IonPage,
    IonTitle,
    IonToolbar,
    IonInput,
    IonButton,
    IonDatetime,
    IonSelectOption, IonSelect, IonAvatar, IonFabButton, IonIcon, IonFab, IonAlert, IonActionSheet
} from '@ionic/react';

import React, {useEffect, useState} from 'react';
import {useHistory, useParams} from 'react-router';
import dataSeleccion from './dataSeleccion.json'
import students from "../Alumnos/dataAlumnos.json";
import {add, closeOutline, createOutline, trash} from "ionicons/icons";
import ImgFromInitials from "../../components/ImgFromInitials";

const Seleccion: React.FC = () => {
    const [showActionSheet, setShowActionSheet] = useState(false);
    const [selectedItem, setSelectedItem] = useState('');
    const [deleteAlert, setDeleteAlert] = useState(false);
    const history = useHistory();   //redireccionamiento

    const displaySheet = (item: any) => {
        setSelectedItem(item);
        setShowActionSheet(true);
    };

    const styleCell = {
        borderColor: 'transparent',
        border: 'transparent'
    };

    const styleRow = {
        borderColor: 'transparent',
        border: 'none'
    };
    const name = "Listar Actividades";
    const [grado, setGrado] = useState('')
    const [seccion, setSeccion] = useState('')
    const [buscar, setBuscar] = useState(false)
    const [selection, setSelection] = useState('3')  //las columnas del grid son 12 entonces 12 / factor que establezca son las que me mostrara

    const [Actividades, setActividades] = useState<any[]>([]);

    async function get() {
        const res = await fetch('https://rickandmortyapi.com/api/character');
        res
            .json()
            .then((res) => {
                setActividades(res.results);
            })
            .catch(err => alert('Ocurio un error.'));
    }

    useEffect(() => {
        get();
    }, []);


    if (Actividades[0]) {
        if (!buscar) {  //no existen actividades
            return (
                <IonPage>
                    <IonHeader>
                        <IonToolbar>
                            <IonButtons slot="start">
                                <IonMenuButton/>
                            </IonButtons>
                        </IonToolbar>
                    </IonHeader>
                    <IonContent>
                        <IonCard>
                            <IonCardHeader>
                                <IonCardTitle
                                    style={{
                                        'font-size': '3em'
                                    }}>Listar Actividades</IonCardTitle>
                            </IonCardHeader>
                        </IonCard>
                        <IonHeader collapse="condense">
                            <IonToolbar>
                                <IonTitle size="large">{name}</IonTitle>
                            </IonToolbar>s
                        </IonHeader>
                        <IonGrid>
                            <IonRow style={styleRow} className="no-border">
                                <IonCol style={styleCell}>
                                    <IonItem>
                                        <IonLabel>Grado: </IonLabel>
                                        <IonInput value={grado} onIonChange={e => setGrado(e.detail.value!)}
                                                  placeholder="Grado"></IonInput>
                                    </IonItem>
                                </IonCol>
                                <IonCol style={styleCell}>
                                    <IonItem>
                                        <IonLabel>Seccion: </IonLabel>
                                        <IonInput value={seccion} onIonChange={e => setSeccion(e.detail.value!)}
                                                  placeholder="Seccion"></IonInput>
                                    </IonItem>
                                </IonCol>
                                <IonCol style={styleCell}>
                                    <div style={{textAlign: 'center'}}>
                                        <IonButton color="warning" shape="round" onClick={() => setBuscar(true)}>Buscar
                                            Actividades</IonButton>
                                    </div>
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                        <IonGrid>
                            <IonRow style={styleRow} className="no-border">
                                <IonCol style={styleCell} size="2"/>
                                <IonCol style={styleCell} size="8">
                                    <IonCard href="" style={
                                        {
                                            width: '100%',
                                            textAlign: 'center',
                                        }}>
                                        <div className='div-img'>
                                            <img
                                                src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTX5pXdfE5Wh5vRlgfON5kTrwZDW2I4sadw79c9ooKnYwKAjzH1Hy0l_XOYeskZomTaT0N-V3QBoU4jgMDKBDOEv-p7-2qOoMnYww&usqp=CAU&ec=45695924"/>
                                        </div>
                                        <IonCardHeader>
                                            <IonCardTitle>NO HAY ACTIVIDADES POR EL MOMENTO</IonCardTitle>
                                        </IonCardHeader>
                                    </IonCard>
                                </IonCol>
                                <IonCol style={styleCell} size="2"/>
                            </IonRow>
                        </IonGrid>
                        <IonFab
                            style={
                                {
                                    'margin': '4%'
                                }
                            }
                            vertical="bottom" horizontal="end" slot="fixed">
                            <IonFabButton routerLink="/crearActividad">
                                <IonIcon icon={add}/>
                            </IonFabButton>
                        </IonFab>
                    </IonContent>
                </IonPage>
            )
        } else {   //existen actividades pendiente si ver si esta de mas o no
            return (
                <IonPage>
                    <IonHeader>
                        <IonToolbar>
                            <IonButtons slot="start">
                                <IonBackButton/>
                            </IonButtons>
                        </IonToolbar>
                    </IonHeader>
                    <IonContent>
                        <IonCard>
                            <IonCardHeader>
                                <IonCardTitle
                                    style={{
                                        'font-size': '3em'
                                    }}>Listar Actividades</IonCardTitle>
                            </IonCardHeader>
                        </IonCard>
                        <IonGrid>
                            <IonRow style={styleRow} className="no-border">
                                <IonCol style={styleCell}>
                                    <IonItem>
                                        <IonLabel>Grado: </IonLabel>
                                        <IonInput value={grado} onIonChange={e => setGrado(e.detail.value!)}
                                                  placeholder="Grado"></IonInput>
                                    </IonItem>
                                </IonCol>
                                <IonCol style={styleCell}>
                                    <IonItem>
                                        <IonLabel>Seccion: </IonLabel>
                                        <IonInput value={seccion} onIonChange={e => setSeccion(e.detail.value!)}
                                                  placeholder="Seccion"></IonInput>
                                    </IonItem>
                                </IonCol>
                                <IonCol style={styleCell}>
                                    <div style={{textAlign: 'center'}}>
                                        <IonButton color="warning" shape="round" onClick={() => setBuscar(true)}>Buscar
                                            Actividades</IonButton>
                                    </div>
                                </IonCol>
                                <IonCol style={styleCell}>
                                    <div style={{textAlign: 'center'}}>
                                        <IonLabel>Vista de actividades por fila</IonLabel>
                                        <IonSelect
                                            interface="alert"
                                            okText="Seleccionar" cancelText="Cancelar" value={selection}
                                            placeholder="Vista de actividades"
                                            onIonChange={e => setSelection(e.detail.value)}>
                                            <IonSelectOption value="3">4 actividades.</IonSelectOption>
                                            <IonSelectOption value="4">3 actividades.</IonSelectOption>
                                            <IonSelectOption value="12">1 actividad.</IonSelectOption>
                                        </IonSelect>
                                        {console.log(selection)}
                                    </div>
                                </IonCol>
                            </IonRow>
                        </IonGrid>

                        <IonGrid size-sm>
                            <IonRow style={styleRow} className="no-border">
                                {
                                    dataSeleccion.map((seleccion, index) => {
                                        return (
                                            <IonCol style={styleCell} size={selection}>
                                                <IonCard onClick={() => displaySheet(index)} key={seleccion.nombre}
                                                         style={
                                                             {
                                                                 width: '95%',
                                                                 marginLeft: '2%'
                                                             }}>
                                                    <div className='div-img' style={{marginRight: 0}}>
                                                        <ImgFromInitials text={seleccion.nombre} fontSize={120} rounded={false}/>
                                                    </div>
                                                    <IonCardHeader>
                                                        <IonCardTitle>{seleccion.nombre}</IonCardTitle>
                                                        <IonCardSubtitle>Actividad AudioVisual</IonCardSubtitle>
                                                    </IonCardHeader>
                                                    <IonCardContent>
                                                        Descripcion de la actividad
                                                    </IonCardContent>
                                                </IonCard>
                                            </IonCol>
                                        )
                                    })
                                }
                            </IonRow>
                        </IonGrid>
                        <IonFab
                            style={
                                {
                                    'margin': '4%'
                                }
                            }
                            vertical="bottom" horizontal="end" slot="fixed">
                            <IonFabButton routerLink="/crearActividad">
                                <IonIcon icon={add}/>
                            </IonFabButton>
                        </IonFab>

                        <IonActionSheet
                            isOpen={showActionSheet}
                            onDidDismiss={() => setShowActionSheet(false)}
                            cssClass='my-custom-class'
                            buttons={[
                                {
                                    text: 'Ver descripcion',
                                    role: 'destructive',
                                    icon: createOutline,
                                    handler: () => {
                                        history.push(`verActividad/${selectedItem}`);
                                    }
                                },
                                {
                                    text: 'Editar',
                                    role: 'destructive',
                                    icon: createOutline,
                                    handler: () => {
                                        history.push(`editarActividad/${selectedItem}`);
                                    }
                                }, {
                                    text: 'Borrar',
                                    role: 'destructive',
                                    icon: trash,
                                    handler: () => {
                                        setDeleteAlert(true);
                                    }
                                }, {
                                    text: 'Cancelar',
                                    icon: closeOutline,
                                    role: 'cancel',
                                    handler: () => {
                                        console.log('Cancel clicked');
                                    }
                                }]}
                        />
                        <IonAlert
                            isOpen={deleteAlert}
                            onDidDismiss={() => setDeleteAlert(false)}
                            cssClass='my-custom-class'
                            header={'Alerta'}
                            message={'¿Seguro que desea eliminar el usuario?'}
                            buttons={[
                                {
                                    text: 'Cancelar',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: blah => {
                                        console.log('Cancelar');
                                    }
                                },
                                {
                                    text: 'Confirmar',
                                    handler: () => {
                                        console.log('Confirmar');
                                    }
                                }
                            ]}
                        />

                    </IonContent>
                </IonPage>
            )
        }
    }else{
        return (
            <div/>
        )
    }
};

export default Seleccion;

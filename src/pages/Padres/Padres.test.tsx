import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import {shallow, configure} from "enzyme";
import { render, fireEvent, getByTestId} from '@testing-library/react'
import Padres from "./Padres";

configure({adapter: new Adapter()});

describe('Padres Tests', () => {
    const container = shallow(<Padres/>);

    it('Deberia Generarse Componente Padres', () => {
        shallow(<Padres/>);
    });

    it('Deberia Tener titulo Padres', async () => {
        const {findByText} = render(<Padres/>);
        await findByText('Padres');
    });

});
import './Padres.css';
import {
    IonButtons,
    IonHeader,
    IonPage,
    IonToolbar,
    IonMenuButton,
    IonContent,
    IonCard,
    IonCardHeader, IonCardTitle, IonList, IonItem, IonAvatar, IonFab, IonFabButton, IonIcon, IonActionSheet, IonAlert
} from "@ionic/react";
import React, {useEffect, useState} from 'react';
import {add, closeOutline, createOutline, trash} from "ionicons/icons";
import {useHistory} from "react-router";
import ImgFromInitials from "../../components/ImgFromInitials";

const Padres: React.FC = () => {
    /*estado del action sheet, se muestra o no se muestra*/
    const [showActionSheet, setShowActionSheet] = useState(false);
    /*Variable que toma el valor del usuario que se clickeo en la lista*/
    const [selectedItem, setSelectedItem] = useState(1);
    /*estado de la alerta, se muestra o no se muestra*/
    const [deleteAlert, setDeleteAlert] = useState(false);

    const history = useHistory();

    const [padres, setPadres] = useState<any[]>([]);

    async function Test() {
        const res = await fetch('https://rickandmortyapi.com/api/character/?species=Human&gender=Male');
        res
            .json()
            .then((res) => {
                setPadres(res.results.slice(0, 5));
            })
            .catch(err => alert('Ocurio un error pendejo'));
    }

    useEffect(() => {
        Test();
    }, []);

    const displaySheet = (item: any) => {
        setSelectedItem(item);
        setShowActionSheet(true);
    };

    if(padres[0]) {
        return (
            <IonPage>
                <IonHeader>
                    <IonToolbar>
                        <IonButtons slot="start">
                            <IonMenuButton/>
                        </IonButtons>
                    </IonToolbar>
                </IonHeader>
                <IonContent>
                    <IonCard>
                        <IonCardHeader>
                            <IonCardTitle
                                style={{
                                    fontSize: '3em'
                                }}>
                                Padres</IonCardTitle>
                        </IonCardHeader>
                        <IonList>
                            {
                                padres.map((padre, index) => {
                                    return (
                                        <IonItem onClick={() => displaySheet(padre.id)}>
                                            <div className='div-img' style={{marginRight: 10}}>
                                                <ImgFromInitials text={padre.name} fontSize={50} rounded={true}/>
                                            </div>
                                            {padre.name}
                                        </IonItem>
                                    )
                                })
                            }
                        </IonList>
                    </IonCard>

                    <IonFab
                        style={
                            {
                                'margin': '4%'
                            }
                        }
                        vertical="bottom" horizontal="end" slot="fixed">
                        <IonFabButton routerLink="/padres/nuevo">
                            <IonIcon icon={add}/>
                        </IonFabButton>
                    </IonFab>

                    <IonActionSheet
                        isOpen={showActionSheet}
                        onDidDismiss={() => setShowActionSheet(false)}
                        cssClass='my-custom-class'
                        buttons={[
                            {
                                text: 'Editar',
                                role: 'destructive',
                                icon: createOutline,
                                handler: () => {
                                    history.push(`padre/${selectedItem}`);
                                }
                            }, {
                                text: 'Borrar',
                                role: 'destructive',
                                icon: trash,
                                handler: () => {
                                    setDeleteAlert(true);
                                }
                            }, {
                                text: 'Cancelar',
                                icon: closeOutline,
                                role: 'cancel',
                                handler: () => {
                                    console.log('Cancel clicked');
                                }
                            }]}
                    >
                    </IonActionSheet>

                    <IonAlert
                        isOpen={deleteAlert}
                        onDidDismiss={() => setDeleteAlert(false)}
                        cssClass='my-custom-class'
                        header={'Alerta'}
                        message={`¿Seguro que desea eliminar el usuario <strong>${padres.filter(padre => padre.id == selectedItem)[0] ? padres.filter(padre => padre.id == selectedItem)[0].name : ''}</strong>?`}
                        buttons={[
                            {
                                text: 'Cancelar',
                                role: 'cancel',
                                cssClass: 'secondary',
                                handler: blah => {
                                    console.log('Cancelar');
                                }
                            },
                            {
                                text: 'Confirmar',
                                handler: () => {
                                    console.log('Confirmar');
                                }
                            }
                        ]}
                    />
                </IonContent>
            </IonPage>
        );
    }else{
        return (
            <div/>
        )
    }
};

export default Padres;

import React, {useEffect, useState} from 'react';
import './Padre.css'
import {
    IonButtons,
    IonHeader,
    IonPage,
    IonToolbar,
    IonBackButton,
    IonContent,
    IonCard,
    IonCardHeader, IonCardTitle, IonItem, IonLabel, IonInput, IonDatetime, IonSelect, IonSelectOption, IonButton
} from "@ionic/react";
import ImgFromInitials from "../../../components/ImgFromInitials";
import {useParams, useRouteMatch} from "react-router";

const Padre: React.FC = () => {

    const [padre, setPadre] = useState<any>(null);
    const {id} = useParams();
    async function Test() {
        const res = await fetch(`https://rickandmortyapi.com/api/character/${id}`);
        res
            .json()
            .then((res) => {
                console.log(res);
                setPadre(res);
            })
            .catch(err => alert('Ocurio un error pendejo'));
    }

    useEffect(() => {
        Test();
    }, []);


    if(padre) {
        return (
            <IonPage>
                <IonHeader>
                    <IonToolbar>
                        <IonButtons slot="start">
                            <IonBackButton/>
                        </IonButtons>
                    </IonToolbar>
                </IonHeader>

                <IonContent>
                    <IonCard style={
                        {
                            top: '50%',
                            position: 'absolute',
                            transform: 'translateY(-50%)',
                            width: '95%',
                            margin: '2.5%',
                            overflow: 'visible'
                        }
                    }>
                        <div style={{
                            textAlign: 'center',
                            marginTop: '-10vh',
                        }}>
                            <div className='div-img' style={{position: "relative", width: '100%'}}>
                                <ImgFromInitials text={padre.name} fontSize={150} rounded={true}/>
                            </div>
                            {/*<img style={{*/}
                            {/*    borderRadius: '50%',*/}
                            {/*    height: '20vh',*/}
                            {/*    width: '20vh'*/}
                            {/*}} src="https://www.independentwestand.org/wp-content/uploads/homer-simpson-5.jpg"/>*/}


                        </div>

                        <IonCardHeader>
                            <IonCardTitle style={{textAlign: 'center'}}>
                                Actualización Padre
                            </IonCardTitle>
                        </IonCardHeader>
                        <IonItem>
                            <IonLabel>Codigo de Identidad</IonLabel>
                            <IonInput placeholder="Código de Identidad" value={padre.id*403822321}
                                      onIonChange={e => setPadre(padre)}></IonInput>
                        </IonItem>
                        <IonItem>
                            <IonLabel>Nombre de Catedrático</IonLabel>
                            <IonInput placeholder="Nombre Catedratico" value={padre.name}
                                      onIonChange={e => setPadre(padre)}></IonInput>
                        </IonItem>
                        <IonItem>
                            <IonLabel>Fecha de Nacimiento</IonLabel>
                            <IonDatetime placeholder="D MMM YYYY" displayFormat="D MMM YYYY" min="1940"
                                         value={padre.created}
                                         onIonChange={e => setPadre(padre)}></IonDatetime>
                        </IonItem>
                        <IonItem>
                            <IonLabel>Institución Educativa</IonLabel>
                            <IonSelect
                                interface="alert"
                                okText="Seleccionar" cancelText="Cancelar" value={padre.location.name}
                                placeholder="Institución Educativa"
                                onIonChange={e => setPadre(padre)}>
                                <IonSelectOption value="0">USAC</IonSelectOption>
                                <IonSelectOption value="1">URL</IonSelectOption>
                                <IonSelectOption value="2">UVG</IonSelectOption>
                            </IonSelect>
                        </IonItem>
                        <div style={{textAlign: 'center'}}>
                            <IonButton color="primary" shape="round"
                                       onClick={() => console.log(padre)}>Actualizar</IonButton>
                        </div>
                    </IonCard>
                </IonContent>
            </IonPage>
        )
    }else {
        return (
            <IonPage>
                <IonHeader>
                    <IonToolbar>
                        <IonButtons slot="start">
                            <IonBackButton/>
                        </IonButtons>
                    </IonToolbar>
                </IonHeader>
            </IonPage>
        )
    }
};

export default Padre;

import {
    IonActionSheet, IonAlert, IonButtons,
    IonCard, IonCardHeader, IonCardTitle,
    IonContent, IonFab, IonFabButton, IonHeader,
    IonIcon, IonItem, IonList, IonMenuButton,
    IonPage, IonTitle, IonToolbar, IonCardSubtitle, IonCardContent,
    IonGrid, IonRow, IonCol, IonLabel, IonButton
} from '@ionic/react';
import React, {useState} from 'react';
import './Home.css';
import { isPlatform } from '@ionic/react';
import {useHistory} from "react-router";

const Home: React.FC = () => {
    let history = useHistory();
    const [selection, setSelection] = useState('3')
    const home = [
        {
            path: '/profile',
            nombre: 'Perfil',
            img: 'https://img.icons8.com/office/80/000000/user.png'
        },{
            path: '/listarInstituciones',
            nombre: 'Intituciones',
            img: 'https://img.icons8.com/office/80/000000/school.png'
        },{
            path: '/maestros',
            nombre: 'Maestros',
            img: 'https://img.icons8.com/office/80/000000/classroom.png'
        },
        {
            path: '/listaralumno',
            nombre: 'Alumnos',
            img: 'https://img.icons8.com/office/80/000000/children.png'
        },
        {
            path: '/padres',
            nombre: 'Padres',
            img: 'https://img.icons8.com/office/80/000000/family--v3.png'
        },
        {
            path: '/reportes',
            nombre: 'Reportes',
            img: 'https://img.icons8.com/office/80/000000/combo-chart.png'
        },
        {
            path: '/actividades',
            nombre: 'Actividades',
            img: 'https://img.icons8.com/office/80/000000/to-do.png'
        }
    ];

    const styleCell = {
        borderColor: 'transparent',
        border: 'transparent'
    };

    const styleRow = {
        borderColor: 'transparent',
        border: 'none'
    };




    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton />
                    </IonButtons>
                </IonToolbar>
            </IonHeader>
            <IonContent>


                {
                    isPlatform('desktop')
                        ?
                        <IonGrid size-sm >
                            <IonRow style={styleRow} >
                                {
                                    home.map((home,index) => {
                                        return(
                                                <IonCol style={styleCell} size='4' key={index}>
                                                    <a onClick= {() => history.push(home.path)}>
                                                        <IonCard key={1} >
                                                            <div className='div-img'>
                                                                <img src={home.img}/>
                                                            </div>
                                                            <IonCardHeader>
                                                                <IonCardSubtitle>{home.nombre}</IonCardSubtitle>
                                                                <IonCardTitle>{home.nombre}</IonCardTitle>
                                                            </IonCardHeader>

                                                            <IonCardContent>
                                                                {home.nombre}
                                                            </IonCardContent>
                                                        </IonCard>
                                                    </a>
                                                </IonCol>
                                        )
                                    })
                                }
                            </IonRow>
                        </IonGrid>



                            :
                        home.map( (home,index) =>{
                            return(
                                <a onClick= {() => history.push(home.path)}>
                                    <IonCard key={index}>
                                        <img src={home.img}/>
                                        <IonCardHeader>
                                            <IonCardSubtitle>{home.nombre}</IonCardSubtitle>
                                            <IonCardTitle>{home.nombre}</IonCardTitle>
                                        </IonCardHeader>

                                        <IonCardContent>
                                            {home.nombre}
                                        </IonCardContent>
                                    </IonCard>
                                </a>
                            )
                        })
                }

            </IonContent>
        </IonPage>
    );
};

export default Home;

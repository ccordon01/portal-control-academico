import {
    IonBackButton,
    IonLabel,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonItem,
    IonButtons,
    IonContent,
    IonHeader,
    IonMenuButton,
    IonPage,
    IonTitle,
    IonToolbar,
    IonInput,
    IonButton,
    IonDatetime,
    IonSelectOption, IonSelect
} from '@ionic/react';
import React, { useState } from 'react';
import { useParams } from 'react-router';
import { useForm, Controller } from "react-hook-form"
import './crearInstitucion.css';
import instituciones from  '../dataInstituciones.json'
import ImgFromInitials from "../../../components/ImgFromInitials";

const CrearInstitucion: React.FC = () => {
    const name = "Crear Institucion";

    const [idInstitucionEducativa, setidInstitucion] = useState<number>()
    const [nombreInstitucionEducativa, setnombreInstitucionEducativa] = useState('')
    const [direccionInstitucionEducativa, setdireccionInstitucionEducativa] = useState('')
    const [contactoInstitucionEducativa, setcontactoInstitucionEducativa] = useState('')
    const [idCatedraticoPrincipal, setidCatedraticoPrincipal] = useState('')
    const [fechaCreacionInstitucionEducativa, setfechaCreacionInstitucionEducativa] = useState('')
    const [fechaUltimaModificacionInstitucionEducativa, setfechaUltimaModificacionInstitucionEducativa ] = useState('')
    const [save, setsave] = useState(false)

    if (save) {
        const Institucion = {
            idInstitucionEducativa: idInstitucionEducativa,
            nombreInstitucionEducativa: nombreInstitucionEducativa,
            direccionInstitucionEducativa: direccionInstitucionEducativa,
            contactoInstitucionEducativa: contactoInstitucionEducativa,
            idCatedraticoPrincipal: idCatedraticoPrincipal,
            fechaCreacionInstitucionEducativa: fechaCreacionInstitucionEducativa,
            fechaUltimaModificacionInstitucionEducativa: fechaUltimaModificacionInstitucionEducativa
        }
        //add to databse
        let stu = instituciones
        stu = {
            ...instituciones,
            ...Institucion
        }
        console.log(stu)
    }
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonBackButton/>
                    </IonButtons>
                    <IonTitle>{name}</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent>
                <IonHeader collapse="condense">
                    <IonToolbar>
                        <IonTitle size="large">{name}</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonCard style={
                    {
                        top: '50%',
                        position: 'absolute',
                        transform: 'translateY(-50%)',
                        width: '95%',
                        margin: '2.5%',
                        overflow: 'visible'
                    }}>
                    <div style={{
                        textAlign: 'center',
                        marginTop: '-10vh',
                    }}>
                        <div className='div-img' style={{position: "relative", width: '100%'}}>
                            <ImgFromInitials text={nombreInstitucionEducativa} fontSize={150} rounded={true}/>
                        </div>
                    </div>
                    <IonCardHeader>
                        <IonCardTitle style = {{textAlign: 'center'}}>Creacion de Alumno</IonCardTitle>
                    </IonCardHeader>
                    <IonItem>
                        <IonLabel>Nombre de la Institucion</IonLabel>
                        <IonInput value={nombreInstitucionEducativa} onIonChange={e => setnombreInstitucionEducativa(e.detail.value!)} placeholder="Nombre de la Institucion"></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel>Direccion Educativa</IonLabel>
                        <IonInput value={direccionInstitucionEducativa} onIonChange={e => setdireccionInstitucionEducativa(e.detail.value!)} placeholder="Direccion De la Institucion"></IonInput>

                    </IonItem>
                    <IonItem>
                        <IonLabel>Contacto Institucion</IonLabel>
                        <IonInput value ={contactoInstitucionEducativa} onIonChange={e => setcontactoInstitucionEducativa(e.detail.value!)} placeholder="Contacto Institucion"></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel>Catedratico Principal</IonLabel>
                        <IonSelect
                            interface="alert"
                            okText="Seleccionar" cancelText="Cancelar" value={idCatedraticoPrincipal}
                            placeholder="Catedratico Principal"
                            onIonChange={e => setidCatedraticoPrincipal(e.detail.value)}>
                            <IonSelectOption value="0">Juan Pablo Osuna</IonSelectOption>
                            <IonSelectOption value="1">Jose Fernando Flores</IonSelectOption>
                        </IonSelect>
                    </IonItem>
                    <div style={{textAlign: 'center'}}>
                        <IonButton color="warning" shape="round" onClick={() => setsave(true)}>Crear</IonButton>
                    </div>
                </IonCard>
            </IonContent>
        </IonPage>
    );
};

export default CrearInstitucion;

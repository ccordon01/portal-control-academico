import {
    IonActionSheet, IonAlert, IonAvatar,
    IonButtons, IonCard, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCol,
    IonContent, IonFab, IonFabButton, IonGrid,
    IonHeader, IonIcon, IonItem, IonList,
    IonMenuButton,
    IonPage, IonRow,
    IonTitle,
    IonToolbar
} from '@ionic/react';
import React, {useEffect, useState} from 'react';
import {
    add,
    arrowBackCircle,
    caretForwardCircle,
    closeOutline,
    createOutline,
    heart,
    share,
    trash
} from "ionicons/icons";
import {useHistory} from "react-router";
import './listarInstituciones.css';
import institutions from '../dataInstituciones.json'
import ImgFromInitials from "../../../components/ImgFromInitials";
console.log(institutions)
const ListarI: React.FC = () => {
    const [showActionSheet, setShowActionSheet] = useState(false);
    const [selectedItem, setSelectedItem] = useState('');
    const [deleteAlert, setDeleteAlert] = useState(false);
    const history = useHistory();   //redireccionamiento

    const displaySheet = (item: any) => {
        setSelectedItem(item);
        setShowActionSheet(true);
    };

    //get data
    const [instituciones, setInstituciones] = useState<any[]>([]);
    async function get() {
        const res = await fetch('https://rickandmortyapi.com/api/character');
        res
            .json()
            .then((res) => {
                setInstituciones(res.results);
            })
            .catch(err => alert('Ocurio un error pendejo'));
    }

    useEffect(() => {
        get();
    }, []);

    if (instituciones[0]) {
        return (
            <IonPage>
                <IonHeader>
                    <IonToolbar>
                        <IonButtons slot="start">
                            <IonMenuButton/>
                        </IonButtons>
                    </IonToolbar>
                </IonHeader>
                <IonContent>
                    <IonCard>
                        <IonCardHeader>
                            <IonCardTitle
                                style={{
                                    'font-size': '3em'
                                }}>Instituciones</IonCardTitle>
                        </IonCardHeader>
                        <IonList>
                            {
                                institutions.map((institucion, index) => {
                                    return (
                                        <IonItem onClick={() => displaySheet(index)}>
                                            <div className ='div-img' style={{marginRight: 10}}>
                                                <ImgFromInitials text={institucion.nombreEstudiante} fontSize={50} rounded={true}/>
                                            </div>
                                            {institucion.nombreEstudiante}
                                        </IonItem>
                                    )
                                })
                            }
                        </IonList>
                    </IonCard>
                    <IonFab
                        style={
                            {
                                'margin': '4%'
                            }
                        }
                        vertical="bottom" horizontal="end" slot="fixed">
                        <IonFabButton routerLink="/crearInstitucion">
                            <IonIcon icon={add}/>
                        </IonFabButton>
                    </IonFab>

                    <IonActionSheet
                        isOpen={showActionSheet}
                        onDidDismiss={() => setShowActionSheet(false)}
                        cssClass='my-custom-class'
                        buttons={[
                            {
                                text: 'Editar',
                                role: 'destructive',
                                icon: createOutline,
                                handler: () => {
                                    history.push(`editarInstitucion/${selectedItem}`);
                                }
                            }, {
                                text: 'Borrar',
                                role: 'destructive',
                                icon: trash,
                                handler: () => {
                                    setDeleteAlert(true);
                                }
                            }, {
                                text: 'Cancelar',
                                icon: closeOutline,
                                role: 'cancel',
                                handler: () => {
                                    console.log('Cancel clicked');
                                }
                            }]}
                    />
                    <IonAlert
                        isOpen={deleteAlert}
                        onDidDismiss={() => setDeleteAlert(false)}
                        cssClass='my-custom-class'
                        header={'Alerta'}
                        message={'¿Seguro que desea eliminar el usuario?'}
                        buttons={[
                            {
                                text: 'Cancelar',
                                role: 'cancel',
                                cssClass: 'secondary',
                                handler: blah => {
                                    console.log('Cancelar');
                                }
                            },
                            {
                                text: 'Confirmar',
                                handler: () => {
                                    console.log('Confirmar');
                                }
                            }
                        ]}
                    />
                </IonContent>
            </IonPage>
        );
    }else{
        return (
            <div/>
        )
    }
};

export default ListarI;

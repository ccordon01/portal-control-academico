export class MaestroModelo {
    idCatedratico?: number;
    nombreCatedratico?: string;
    fechaNacimientoCatedratico?: string;
    codigoIdentidadCatedratico?: number;
    idTipoCatedratico?: number;
    fechaCreacionCatedratico?: string;
    fechaUltimaModificacionCatedratico?: string;
    idUsuarioCatedratico?: number;
    idInstitucionEducativaCatedratico?: number;
}
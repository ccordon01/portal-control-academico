import {
    IonButton,
    IonButtons, IonCard, IonCardHeader, IonCardTitle,
    IonContent,
    IonHeader,
    IonInput, IonItem,
    IonBackButton,
    IonPage,
    IonTitle,
    IonToolbar, IonDatetime, IonLabel, IonSelect, IonSelectOption, IonAvatar
} from '@ionic/react';
import React, {useEffect, useState} from 'react';
import './Maestro.css';
import {useParams} from "react-router";
import ImgFromInitials from "../../../components/ImgFromInitials";

const Maestro: React.FC = () => {
    const name = "Maestro";

    const [maestro, setMaestro] = useState<any>(null);
    const {id} = useParams();
    async function Test() {
        const res = await fetch(`https://rickandmortyapi.com/api/character/${id}`);
        res
            .json()
            .then((res) => {
                console.log(res);
                setMaestro(res);
            })
            .catch(err => alert('Ocurio un error pendejo'));
    }

    useEffect(() => {
        Test();
    }, []);


    if(maestro) {
        return (
            <IonPage>
                <IonHeader>
                    <IonToolbar>
                        <IonButtons slot="start">
                            <IonBackButton/>
                        </IonButtons>
                        <IonTitle></IonTitle>
                    </IonToolbar>
                </IonHeader>

                <IonContent>
                    <IonCard style={
                        {
                            top: '50%',
                            position: 'absolute',
                            transform: 'translateY(-50%)',
                            width: '95%',
                            margin: '2.5%',
                            overflow: 'visible'
                        }}>
                        <div style={{
                            textAlign: 'center',
                            marginTop: '-10vh',
                        }}>
                            <div className='div-img' style={{position: "relative", width: '100%'}}>
                                <ImgFromInitials text={maestro.name} fontSize={150} rounded={true}/>
                            </div>
                        </div>
                        <IonCardHeader>
                            <IonCardTitle style={{textAlign: 'center'}}>
                                Actualización Maestro
                            </IonCardTitle>
                        </IonCardHeader>
                        <IonItem>
                            <IonLabel>Codigo de Identidad</IonLabel>
                            <IonInput placeholder="Código de Identidad" value={maestro.id*20930234324}
                                      onIonChange={e => setMaestro(maestro)}></IonInput>
                        </IonItem>
                        <IonItem>
                            <IonLabel>Nombre de Catedrático</IonLabel>
                            <IonInput placeholder="Nombre Catedratico" value={maestro.name}
                                      onIonChange={e => setMaestro(maestro)}></IonInput>
                        </IonItem>
                        <IonItem>
                            <IonLabel>Fecha de Nacimiento</IonLabel>
                            <IonDatetime placeholder="D MMM YYYY" displayFormat="D MMM YYYY" min="1940"
                                         value={maestro.created}
                                         onIonChange={e => setMaestro(maestro)}></IonDatetime>
                        </IonItem>
                        <IonItem>
                            <IonLabel>Tipo Catedrático</IonLabel>
                            <IonSelect
                                interface="alert"
                                okText="Seleccionar" cancelText="Cancelar" value={maestro.specie}
                                placeholder="Tipo Catedrático"
                                onIonChange={e => setMaestro(maestro)}>
                                <IonSelectOption value="0">Titular</IonSelectOption>
                                <IonSelectOption value="1">Interino</IonSelectOption>
                            </IonSelect>
                        </IonItem>
                        <IonItem>
                            <IonLabel>Institución Educativa</IonLabel>
                            <IonSelect
                                interface="alert"
                                okText="Seleccionar" cancelText="Cancelar" value={maestro.location}
                                placeholder="Institución Educativa"
                                onIonChange={e => setMaestro(maestro)}>
                                <IonSelectOption value="0">USAC</IonSelectOption>
                                <IonSelectOption value="1">URL</IonSelectOption>
                                <IonSelectOption value="2">UVG</IonSelectOption>
                            </IonSelect>
                        </IonItem>
                        <div style={{textAlign: 'center'}}>
                            <IonButton color="primary" shape="round"
                                       onClick={() => console.log(maestro)}>Actualizar</IonButton>
                        </div>
                    </IonCard>
                </IonContent>
            </IonPage>
        );
    }else{
        return (
            <IonPage>
                <IonHeader>
                    <IonToolbar>
                        <IonButtons slot="start">
                            <IonBackButton/>
                        </IonButtons>
                    </IonToolbar>
                </IonHeader>
            </IonPage>
        )
    }
};

export default Maestro;
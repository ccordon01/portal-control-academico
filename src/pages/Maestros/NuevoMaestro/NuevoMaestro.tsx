import {
    IonButton,
    IonButtons, IonCard, IonCardHeader, IonCardTitle,
    IonContent,
    IonHeader,
    IonInput, IonItem,
    IonBackButton,
    IonPage,
    IonTitle,
    IonToolbar, IonDatetime, IonLabel, IonSelect, IonSelectOption
} from '@ionic/react';
import React, {useState} from 'react';
import './NuevoMaestro.css';
import {useParams} from "react-router";

const NuevoMaestro: React.FC = () => {
    const name = "Profesor";

    // const[modelo, setModelo] = useState<MaestroModelo>({
    //     idCatedratico: 1,
    //     nombreCatedratico: 'Mi nombre',
    //     fechaNacimientoCatedratico: '',
    //     codigoIdentidadCatedratico: 1,
    //     idTipoCatedratico: 3,
    //     fechaCreacionCatedratico: '',
    //     fechaUltimaModificacionCatedratico: '',
    //     idUsuarioCatedratico: 4,
    //     idInstitucionEducativaCatedratico: 8,
    // });

    const [idCatedratico, setIdCatedratico] = useState('');
    const [nombreCatedratico, setNombreCatedratico] = useState('');
    const [fechaNacimientoCatedratico, setfechaNacimientoCatedratico] = useState('');
    const [codigoIdentidadCatedratico, setcodigoIdentidadCatedratico] = useState('');
    const [idTipoCatedratico, setidTipoCatedratico] = useState('');
    const [fechaCreacionCatedratico, setfechaCreacionCatedratico] = useState('');
    const [fechaUltimaModificacionCatedratico, setfechaUltimaModificacionCatedratico] = useState('');
    const [idUsuarioCatedratico, setidUsuarioCatedratico] = useState('');
    const [idInstitucionEducativaCatedratico, setidInstitucionEducativaCatedratico] = useState('');

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonBackButton/>
                    </IonButtons>
                    <IonTitle></IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent>
                <IonCard style={
                    {
                        top: '50%',
                        position: 'absolute',
                        transform: 'translateY(-50%)',
                        width: '95%',
                        margin: '2.5%',
                        overflow: 'visible'
                    }}>
                    <div style={{
                        textAlign: 'center',
                        marginTop: '-10vh',
                    }}>
                        <img style={{
                            borderRadius: '50%',
                            height: '20vh',
                            width: '20vh'
                        }} src="https://pbs.twimg.com/profile_images/2783958225/9b26264c4ff8970b4d49a2d99b245ecb.png" />
                    </div>
                    <IonCardHeader>
                        <IonCardTitle style={{textAlign: 'center'}}>
                            Nuevo Maestro
                        </IonCardTitle>
                    </IonCardHeader>
                    <IonItem>
                        <IonLabel>Codigo de Identidad</IonLabel>
                        <IonInput placeholder="Código de Identidad" value={codigoIdentidadCatedratico}
                                  onIonChange={e => setcodigoIdentidadCatedratico(e.detail.value!)}></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel>Nombre de Catedrático</IonLabel>
                        <IonInput placeholder="Nombre Catedratico" value={nombreCatedratico}
                                  onIonChange={e => setNombreCatedratico(e.detail.value!)}></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel>Fecha de Nacimiento</IonLabel>
                        <IonDatetime placeholder="D MMM YYYY" displayFormat="D MMM YYYY" min="1940" value={fechaNacimientoCatedratico}
                                     onIonChange={e => setfechaNacimientoCatedratico(e.detail.value!)}></IonDatetime>
                    </IonItem>
                    <IonItem>
                        <IonLabel>Tipo Catedrático</IonLabel>
                        <IonSelect
                            interface="alert"
                            okText="Seleccionar" cancelText="Cancelar" value={idTipoCatedratico}
                            placeholder="Tipo Catedrático"
                            onIonChange={e => setidTipoCatedratico(e.detail.value)}>
                            <IonSelectOption value="0">Titular</IonSelectOption>
                            <IonSelectOption value="1">Interino</IonSelectOption>
                        </IonSelect>
                    </IonItem>
                    <IonItem>
                        <IonLabel>Institución Educativa</IonLabel>
                        <IonSelect
                            interface="alert"
                            okText="Seleccionar" cancelText="Cancelar" value={idInstitucionEducativaCatedratico}
                            placeholder="Institución Educativa"
                            onIonChange={e => setidInstitucionEducativaCatedratico(e.detail.value)}>
                            <IonSelectOption value="0">USAC</IonSelectOption>
                            <IonSelectOption value="1">URL</IonSelectOption>
                            <IonSelectOption value="2">UVG</IonSelectOption>
                        </IonSelect>
                    </IonItem>
                    <div style={{textAlign: 'center'}}>
                        <IonButton color="warning" shape="round"
                                   onClick={() => console.log(fechaNacimientoCatedratico)}>Crear</IonButton>
                    </div>
                </IonCard>
            </IonContent>
        </IonPage>
    );
};

export default NuevoMaestro;
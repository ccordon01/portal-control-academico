import {
    IonActionSheet, IonAlert, IonAvatar, IonButtons,
    IonCard, IonCardHeader, IonCardTitle,
    IonContent, IonFab, IonFabButton, IonHeader,
    IonIcon, IonImg, IonItem, IonList, IonMenuButton,
    IonPage, IonTitle, IonToolbar,
} from '@ionic/react';
import React, {useEffect, useState} from 'react';
import './Maestros.css';
import {
    add,
    closeOutline,
    createOutline,
    trash
} from "ionicons/icons";
import {useHistory} from "react-router";
import ImgFromInitials from "../../components/ImgFromInitials";

const Maestros: React.FC = () => {
    /*estado del action sheet, se muestra o no se muestra*/
    const [showActionSheet, setShowActionSheet] = useState(false);
    /*Variable que toma el valor del usuario que se clickeo en la lista*/
    const [selectedItem, setSelectedItem] = useState(1);
    /*estado de la alerta, se muestra o no se muestra*/
    const [deleteAlert, setDeleteAlert] = useState(false);

    const [maestros, setMaestros] = useState<any[]>([]);

    const history = useHistory();

    async function Test() {
        const res = await fetch('https://rickandmortyapi.com/api/character/?species=Human&gender=Female');
        res
            .json()
            .then((res) => {
                setMaestros(res.results.slice(0, 5));
            })
            .catch(err => alert('Ocurio un error pendejo'));
    }

    useEffect(() => {
        Test();
    }, []);

    const name = "Maestros";


    const displaySheet = (item: any) => {
        setSelectedItem(item);
        setShowActionSheet(true);
    };

    if(maestros[0]) {
        return (
            <IonPage>
                <IonHeader>
                    <IonToolbar>
                        <IonButtons slot="start">
                            <IonMenuButton/>
                        </IonButtons>
                    </IonToolbar>
                </IonHeader>
                <IonContent>
                    <IonCard>
                        <IonCardHeader>
                            <IonCardTitle
                                style={{
                                    'font-size': '3em'
                                }}>Maestros</IonCardTitle>
                        </IonCardHeader>
                        <IonList>
                            {
                                maestros.map((maestro, index) => {
                                    return (
                                        <IonItem onClick={() => displaySheet(maestro.id)}>
                                            <div className='div-img' style={{marginRight: 10}}>
                                                <ImgFromInitials text={maestro.name} fontSize={50} rounded={true}/>
                                            </div>
                                            {maestro.name}
                                        </IonItem>
                                    )
                                })
                            }
                        </IonList>
                    </IonCard>

                    <IonFab
                        style={
                            {
                                'margin': '4%'
                            }
                        }
                        vertical="bottom" horizontal="end" slot="fixed">
                        <IonFabButton routerLink="/maestros/nuevo">
                            <IonIcon icon={add}/>
                        </IonFabButton>
                    </IonFab>

                    <IonActionSheet
                        isOpen={showActionSheet}
                        onDidDismiss={() => setShowActionSheet(false)}
                        cssClass='my-custom-class'
                        buttons={[
                            {
                                text: 'Editar',
                                role: 'destructive',
                                icon: createOutline,
                                handler: () => {
                                    history.push(`maestro/${selectedItem}`);
                                }
                            }, {
                                text: 'Borrar',
                                role: 'destructive',
                                icon: trash,
                                handler: () => {
                                    setDeleteAlert(true);
                                }
                            }, {
                                text: 'Cancelar',
                                icon: closeOutline,
                                role: 'cancel',
                                handler: () => {
                                    console.log('Cancel clicked');
                                }
                            }]}
                    >
                    </IonActionSheet>

                    <IonAlert
                        isOpen={deleteAlert}
                        onDidDismiss={() => setDeleteAlert(false)}
                        cssClass='my-custom-class'
                        header={'Alerta'}
                        message={`¿Seguro que desea eliminar el usuario <strong>${maestros.filter(maestro => maestro.id == selectedItem)[0] ? maestros.filter(maestro => maestro.id == selectedItem)[0].name : ''}</strong>?`}
                        buttons={[
                            {
                                text: 'Cancelar',
                                role: 'cancel',
                                cssClass: 'secondary',
                                handler: blah => {
                                    console.log('Cancelar');
                                }
                            },
                            {
                                text: 'Confirmar',
                                handler: () => {
                                    console.log('Confirmar');
                                }
                            }
                        ]}
                    />

                </IonContent>
            </IonPage>
        );
    }else{
        return (
            <div/>
        )
    }
};

export default Maestros;
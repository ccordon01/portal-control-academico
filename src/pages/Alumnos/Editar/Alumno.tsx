import {
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonItem,
    IonButtons,
    IonContent,
    IonHeader,
    IonMenuButton,
    IonPage,
    IonTitle,
    IonToolbar,
    IonInput,
    IonButton,
    IonLabel,
    IonSelect, IonSelectOption, IonBackButton, IonDatetime
} from '@ionic/react';
import React, { useState } from 'react';
import './Alumno.css';
import { useParams } from 'react-router';
import { Alumnomodelo } from '../Modelos/AlumnoModelo';
import { useForm, Controller } from "react-hook-form"
import ImgFromInitials from "../../../components/ImgFromInitials";

const Alumno: React.FC = () => {
    const name = "Alumno";
    const { id } = useParams<{ id: string; }>();

    /*const [student, setEstudent] = useState<Alumnomodelo>({
        idEstudiante: +id,
        nombreEstudiante: 'test',
        fechaNacimientoEstudiante: '17/8/2020',
        gradoActualEstudiante: 1,
        seccionActualEstudiante: 'A',
        fechaCreacionEstudiante: '1/1/2019',
        fechaUltimaModificacionestudiante: '17/1/2020',
        idUsuarioEstudiante: 2
    });*/

    const [idEstudiante, setidEstudiante] = useState(1)                                                     //no podra editarse
    const [nombreEstudiante, setNombreEstudent] = useState('')
    const [fechaNacimientoEstudiante, setfechaNacimientoEstudiante] = useState('')
    const [gradoActualEstudiante, setgradoActualEstudiante] = useState(1)
    const [seccionActualEstudiante, setseccionActualEstudiante] = useState('')
    const [fechaCreacionEstudiante, setfechaCreacionEstudiante] = useState('')
    const [fechaUltimaModificacionestudiante, setfechaUltimaModificacionestudiante] = useState('')          //se debe actualizar automaticamente
    const [idUsuarioEstudiante, setidUsuarioEstudiante] = useState(2)


    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonBackButton/>
                    </IonButtons>
                    <IonTitle>{name}</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent>
                <IonHeader collapse="condense">
                    <IonToolbar>
                        <IonTitle size="large">{name}</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonCard style={
                    {
                        top: '50%',
                        position: 'absolute',
                        transform: 'translateY(-50%)',
                        width: '95%',
                        margin: '2.5%',
                        overflow: 'visible'
                    }}>
                    <div style={{
                        textAlign: 'center',
                        marginTop: '-10vh',
                    }}>
                        <div className='div-img' style={{position: "relative", width: '100%'}}>
                            <ImgFromInitials text={nombreEstudiante} fontSize={150} rounded={true}/>
                        </div>
                    </div>
                    <IonCardHeader>
                        <IonCardTitle style = {{textAlign: 'center'}}>Actualizacion de Alumno</IonCardTitle>
                    </IonCardHeader>
                    <IonItem>
                        <IonLabel>Nombre del Estudiante</IonLabel>
                        <IonInput value={nombreEstudiante} onIonChange={e => setNombreEstudent(e.detail.value!)} placeholder="nombre del estudiante"></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel>Id Estudiante</IonLabel>
                        <IonInput  value ={idEstudiante} onIonChange={e => setidEstudiante(+e.detail.value!)} placeholder="id del estudiante"></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel>Fecha de Nacimiento</IonLabel>
                        <IonDatetime placeholder="D MMM YYYY" displayFormat="D MMM YYYY" min="1940" value={fechaNacimientoEstudiante}
                                     onIonChange={e => setfechaNacimientoEstudiante(e.detail.value!)}></IonDatetime>
                    </IonItem>
                    <IonItem>
                        <IonLabel>Grado Actual</IonLabel>
                        <IonInput value ={gradoActualEstudiante} onIonChange={e => setgradoActualEstudiante(+e.detail.value!)} placeholder="grado actual"></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel>Seccion Actual</IonLabel>
                        <IonInput value ={seccionActualEstudiante} onIonChange={e => setseccionActualEstudiante(e.detail.value!)} placeholder="seccion"></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel>Fecha Creacion</IonLabel>
                        <IonDatetime placeholder="D MMM YYYY" displayFormat="D MMM YYYY" min="1940" value={fechaCreacionEstudiante}
                                     onIonChange={e => setfechaCreacionEstudiante(e.detail.value!)}></IonDatetime>
                    </IonItem>
                    <IonItem>
                        <IonLabel>Usuario Encargado</IonLabel>
                        <IonSelect
                            interface="alert"
                            okText="Seleccionar" cancelText="Cancelar" value={idUsuarioEstudiante}
                            placeholder="Usuario Encargado"
                            onIonChange={e => setidUsuarioEstudiante(e.detail.value)}>
                            <IonSelectOption value="0">Juan Pablo Osuna</IonSelectOption>
                            <IonSelectOption value="1">Jose Fernando Flores</IonSelectOption>
                        </IonSelect>
                    </IonItem>
                    <div style={{textAlign: 'center'}}>
                        <IonButton color="primary" shape="round" onClick={() => console.log(nombreEstudiante)}>Actualizar</IonButton>
                    </div>
                    
                </IonCard>
            </IonContent>
        </IonPage>
    );
};

export default Alumno;

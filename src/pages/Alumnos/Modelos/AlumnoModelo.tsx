export class Alumnomodelo{
    idEstudiante ?: number;
    nombreEstudiante ?: string;
    fechaNacimientoEstudiante ?: string;
    gradoActualEstudiante ?: number;
    seccionActualEstudiante ?: string;
    fechaCreacionEstudiante ?: string;
    fechaUltimaModificacionestudiante ?: string;
    idUsuarioEstudiante ?: number;
}
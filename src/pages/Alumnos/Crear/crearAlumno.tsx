import {
    IonBackButton,
    IonLabel,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonItem,
    IonButtons,
    IonContent,
    IonHeader,
    IonMenuButton,
    IonPage,
    IonTitle,
    IonToolbar,
    IonInput,
    IonButton,
    IonDatetime,
    IonSelectOption, IonSelect
} from '@ionic/react';
import React, { useState } from 'react';
import { useParams } from 'react-router';
import { useForm, Controller } from "react-hook-form"
import { Alumnomodelo } from '../Modelos/AlumnoModelo';
import './crearAlumno.css';
import students from  '../dataAlumnos.json'
import ImgFromInitials from "../../../components/ImgFromInitials";

const CrearAlumno: React.FC = () => {
    const name = "Crear Alumno";

    const [idEstudiante, setidEstudiante] = useState<number>()
    const [nombreEstudiante, setNombreEstudent] = useState('')
    const [fechaNacimientoEstudiante, setfechaNacimientoEstudiante] = useState('')
    const [gradoActualEstudiante, setgradoActualEstudiante] = useState<number>()
    const [seccionActualEstudiante, setseccionActualEstudiante] = useState('')
    const [fechaCreacionEstudiante, setfechaCreacionEstudiante] = useState('')
    const [fechaUltimaModificacionestudiante, setfechaUltimaModificacionestudiante] = useState('')
    const [idUsuarioEstudiante, setidUsuarioEstudiante] = useState<number>()
    const [save, setsave] = useState(false)

    if (save) {
        const estudiante: Alumnomodelo = {
            idEstudiante: idEstudiante,                                             //se lo asignara la base de datos
            nombreEstudiante: nombreEstudiante,
            fechaNacimientoEstudiante: fechaNacimientoEstudiante,
            gradoActualEstudiante: gradoActualEstudiante,
            seccionActualEstudiante: seccionActualEstudiante,
            fechaCreacionEstudiante: fechaCreacionEstudiante,                       //debera asignarse automaticamente
            fechaUltimaModificacionestudiante: fechaUltimaModificacionestudiante,   //debera asignarse automaticamente
            idUsuarioEstudiante: idUsuarioEstudiante
        }
        //add to databse
        let stu = students
        stu = {
            ...students,
            ...estudiante
        }
        console.log(stu)
    }
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonBackButton/>
                    </IonButtons>
                    <IonTitle>{name}</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent>
                <IonHeader collapse="condense">
                    <IonToolbar>
                        <IonTitle size="large">{name}</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonCard style={
                    {
                        top: '50%',
                        position: 'absolute',
                        transform: 'translateY(-50%)',
                        width: '95%',
                        margin: '2.5%',
                        overflow: 'visible'
                    }}>
                    <div style={{
                        textAlign: 'center',
                        marginTop: '-10vh',
                    }}>
                        <div className='div-img' style={{position: "relative", width: '100%'}}>
                            <ImgFromInitials text={nombreEstudiante} fontSize={150} rounded={true}/>
                        </div>
                    </div>
                    <IonCardHeader>
                        <IonCardTitle style = {{textAlign: 'center'}}>Creacion de Alumno</IonCardTitle>
                    </IonCardHeader>
                    <IonItem>
                        <IonLabel>Nombre del Estudiante</IonLabel>
                        <IonInput value={nombreEstudiante} onIonChange={e => setNombreEstudent(e.detail.value!)} placeholder="nombre del estudiante"></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel>Fecha de Nacimiento</IonLabel>
                        <IonDatetime placeholder="D MMM YYYY" displayFormat="D MMM YYYY" min="1940" value={fechaNacimientoEstudiante}
                                     onIonChange={e => setfechaNacimientoEstudiante(e.detail.value!)}></IonDatetime>
                    </IonItem>
                    <IonItem>
                        <IonLabel>Grado del Estudiante</IonLabel>
                        <IonInput value ={gradoActualEstudiante} onIonChange={e => setgradoActualEstudiante(+e.detail.value!)} placeholder="grado actual"></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel>Seccion</IonLabel>
                        <IonInput value ={seccionActualEstudiante} onIonChange={e => setseccionActualEstudiante(e.detail.value!)} placeholder="seccion"></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel>Usuario Encargado</IonLabel>
                        <IonSelect
                            interface="alert"
                            okText="Seleccionar" cancelText="Cancelar" value={idUsuarioEstudiante}
                            placeholder="Usuario Encargado"
                            onIonChange={e => setidUsuarioEstudiante(e.detail.value)}>
                            <IonSelectOption value="0">Juan Pablo Osuna</IonSelectOption>
                            <IonSelectOption value="1">Jose Fernando Flores</IonSelectOption>
                        </IonSelect>
                    </IonItem>
                    <div style={{textAlign: 'center'}}>
                        <IonButton color="warning" shape="round" onClick={() => setsave(true)}>Crear</IonButton>
                    </div>
                </IonCard>
            </IonContent>
        </IonPage>
    );
};

export default CrearAlumno;

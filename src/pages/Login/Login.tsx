import {
    IonButtons, IonCol,
    IonContent, IonHeader,
    IonMenuButton,
    IonPage, IonRow,
    IonTitle,
    IonToolbar,
    IonItem, IonLabel, IonInput, IonButton,
    IonCard,IonCardContent,
} from '@ionic/react';
import React, { useState }from 'react';
import './Login.css';
import { isPlatform } from '@ionic/react';


import { useHistory } from "react-router-dom";


const Login: React.FC = () => {
    let history = useHistory();
    const [ email, setEmail ] = useState('');
    const [ password, setPassword ] = useState('');
    const [isAuthenticated, userHasAuthenticated] = useState(false);


    const name = "Login";
    const styleImg = {
        display: "block",
        marginLeft: "auto",
        marginRight: "auto",
        marginTop: '2%',
        width: '75%'
    }

    let  styleIonCard = {
        top: '30%',
        position: 'absolute',
        transform: 'translateY(-50%)',
        width: '70%',
        margin: '15%',
    }

    let  styleIonInput = {
        textAlign:'left'
    }

    const styleIonButton = {
    }

    if(isPlatform('mobileweb') || isPlatform('mobile') || isPlatform('android')){
        styleIonCard = {
            top: 'null',
            position: 'null',
            transform: 'null',
            width: 'null',
            margin: 'null',
        };
    }else if(isPlatform('desktop')){

    }

    const submit = async () => {
        history.push('/home');
        const data = {
            token: '12345678'
        }
        localStorage.setItem('token',JSON.stringify(data));
        userHasAuthenticated(true);
        /*const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({email: email, password: password})
        };
        const response = await fetch('https://reqres.in/api/login', requestOptions);
        const data = await response.json();

        localStorage.setItem('token',JSON.stringify(data));*/

        setEmail("");
        setPassword("");
    }



    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton/>
                    </IonButtons>
                    <IonTitle>{name}</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent>


                <IonCard style={styleIonCard}>

                    {
                        isPlatform('desktop')
                        ?
                            <img style={styleImg} src='assets/icon/web.png'/>
                        :
                            <img style={styleImg} src='assets/icon/mobile.png'/>
                    }

                    <IonItem>
                        <IonLabel></IonLabel>
                    </IonItem>

                    <IonCardContent>
                        <form onSubmit={(e) => { e.preventDefault(); submit();}}>
                            <IonItem lines="full">
                                <IonLabel position="floating" >Email</IonLabel>
                                <IonInput style={styleIonInput} name="email"  placeholder='email@example.com' type="email" value={email} onIonChange={(e) => setEmail(e.detail.value!)} required/>
                            </IonItem>

                            <IonItem lines="full">
                                <IonLabel position="floating">Password</IonLabel>
                                <IonInput style={styleIonInput} name="password"  placeholder='Enter password' type="password" value={password} onIonChange={(e) => setPassword(e.detail.value!)} required/>
                            </IonItem>
                            <br/>

                            <IonButton type="submit" color="danger" expand="block"  shape='round' style={styleIonButton}> Sign In</IonButton>
                            <a className="small-text">Forgot Password?</a>
                        </form>
                    </IonCardContent>
                </IonCard>
            </IonContent>
        </IonPage>
    );
};

export default Login;
